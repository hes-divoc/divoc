# diVoc

Mobile application for virus dissemination prevention. Divoc makes your phone ring whenever the distance between people is less than 2m. Divoc keeps your privacy. Whenever you leave the house or go to work.